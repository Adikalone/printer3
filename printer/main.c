/* main.c */

#include <string.h>

#include "ast.h"
#include "ring.h"
#include "tokenizer.h"

void ast_print(struct AST ast) { print_ast(stdout, &ast); }
void token_print(struct token token) { printf("%s(%" PRId64 ")", TOKENS_STR[token.type], token.value); }

DECLARE_RING(ast, struct AST)
DEFINE_RING(ast, struct AST)
DEFINE_RING_PRINT(ast, ast_print)
DEFINE_RING(token, struct token)
DEFINE_RING_PRINT(token, token_print)



const enum binop_type TOKENS_TO_AST[] = {
    [TOK_PLUS] = BIN_PLUS, 
    [TOK_MINUS] = BIN_MINUS, 
    [TOK_MUL] = BIN_MUL, 
    [TOK_DIV] = BIN_DIV, 
    };

#define RETURN_ERROR(code, msg) return printf(msg), code

struct AST *on_one_level(struct ring_token *token);

struct AST *build_ast(char *str)
{
  struct ring_token *tokens = NULL;
  if ((tokens = tokenize(str)) == NULL)
    RETURN_ERROR(NULL, "Tokenization error.\n");

  ring_token_print(tokens);
  
  struct ring_token* current = tokens->next;

  struct AST *ast = on_one_level(current); 
    ring_token_free(&tokens);

  return ast;
}

struct AST *on_one_level(struct ring_token* token){
  struct AST *ast = NULL;
  int level = 0;
  struct ring_token* start = token;
  struct ring_token* next = NULL;
  struct ring_token* prev = NULL;
  while(token != start && token->value.type != TOK_END ){
    next = token->next;
    prev = token->prev;
    if(token->value.type == TOK_PLUS ||
      token->value.type == TOK_MINUS ||
      token->value.type == TOK_DIV ||
      token->value.type == TOK_MUL
    ){
      struct AST *left, *right = NULL;
      if(next->value.type == TOK_OPEN)
                  right = on_one_level(next->next); 
      else if (next->value.type == TOK_NEG)
          right = unop(UN_NEG, lit(next->next->value.value));
      else
       right = lit(next->value.value);
      if(prev->value.type == TOK_CLOSE)
                  left = on_one_level(prev->prev); 
      else if (prev->prev->value.type == TOK_NEG)
          left = unop(UN_NEG, lit(prev->value.value));
      else
       left = lit(prev->value.value);
      ast = binop(TOKENS_TO_AST[token->value.type], left, right);
      return ast;
    }
    if(token->value.type == TOK_OPEN){
      level++;
    }
    if(token->value.type == TOK_CLOSE){
      if(level == 0){
        printf("Build ast error");
        return NULL;
      }
      else {
        level--;
      }
    }
  }
  return NULL;
}








int main()
{
  //char *str = "1 + 2 * (2 - -3) + 8";
  const int MAX_LEN = 1024;
  char str[MAX_LEN];
  if (fgets(str, MAX_LEN, stdin) == NULL)
    RETURN_ERROR(0, "Input is empty.");

  if (str[strlen(str) - 1] == '\n')
    str[strlen(str) - 1] = '\0';

  struct AST *ast = build_ast(str);

  if (ast == NULL)
    printf("AST build error.\n");
  else
  {
    print_ast(stdout, ast);
    printf("\n\n%s = %" PRId64 "\n", str, calc_ast(ast));
    p_print_ast(stdout, ast);
    printf(" = %" PRId64 "\n", calc_ast(ast));    
  }

  return 0;
}
