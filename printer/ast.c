/* ast.c */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ast.h"

struct AST *newnode(struct AST ast) {
  struct AST *const node = malloc(sizeof(struct AST));
  *node = ast;
  return node;
}

struct AST _lit(int64_t value) {
  return (struct AST){AST_LIT, .as_literal = {value}};
}

struct AST *lit(int64_t value) {
  return newnode(_lit(value));
}
struct AST _unop(enum unop_type type, struct AST *operand) {
  return (struct AST){AST_UNOP, .as_unop = {type, operand}};
}

struct AST *unop(enum unop_type type, struct AST *operand) {
  return newnode(_unop(type, operand));
}

struct AST _binop(enum binop_type type, struct AST *left, struct AST *right) {
  return (struct AST){AST_BINOP, .as_binop = {type, left, right}};
}
struct AST *binop(enum binop_type type, struct AST *left, struct AST *right) {
  return newnode(_binop(type, left, right));
}

static const char *BINOPS[] = {
    [BIN_PLUS] = "+", [BIN_MINUS] = "-", [BIN_MUL] = "*", [BIN_DIV] = "/" };
static const char *UNOPS[] = {[UN_NEG] = "-"};

typedef void(printer)(FILE *, struct AST *);


static void print_binop(FILE *f, struct AST *ast) {
  fprintf(f, "(");
  print_ast(f, ast->as_binop.left);
  fprintf(f, ")");
  fprintf(f, "%s", BINOPS[ast->as_binop.type]);
  fprintf(f, "(");
  print_ast(f, ast->as_binop.right);
  fprintf(f, ")");
}
static void print_unop(FILE *f, struct AST *ast) {
  fprintf(f, "%s(", UNOPS[ast->as_unop.type]);
  print_ast(f, ast->as_unop.operand);
  fprintf(f, ")");
}
static void print_lit(FILE *f, struct AST *ast) {
  fprintf(f, "%" PRId64, ast->as_literal.value);
}


static printer *ast_printers[] = {
    [AST_BINOP] = print_binop, [AST_UNOP] = print_unop, [AST_LIT] = print_lit };

void print_ast(FILE *f, struct AST *ast) {
  if (ast)
    ast_printers[ast->type](f, ast);
  else
    fprintf(f, "<NULL>");
}

typedef int64_t(calc)(struct AST *ast);

int64_t calc_ast_binop(struct AST *ast);
int64_t calc_ast_upon(struct AST *ast);
int64_t calc_ast_lit(struct AST *ast);



static calc *calc_ast_various[] = {
  [AST_BINOP] = calc_ast_binop, [AST_UNOP] = calc_ast_upon, [AST_LIT] = calc_ast_lit };

int64_t calc_ast_binop(struct AST *ast){
  int64_t result = 0;
  if(ast->as_binop.type == BIN_PLUS){
    result += calc_ast_various[ast->as_binop.left->type](ast->as_binop.left) + calc_ast_various[ast->as_binop.right->type](ast->as_binop.right);  
  }
  if(ast->as_binop.type == BIN_MINUS){
    result += calc_ast_various[ast->as_binop.left->type](ast->as_binop.left) - calc_ast_various[ast->as_binop.right->type](ast->as_binop.right);  
  }
  if(ast->as_binop.type == BIN_MUL){
    result += calc_ast_various[ast->as_binop.left->type](ast->as_binop.left) * calc_ast_various[ast->as_binop.right->type](ast->as_binop.right);  
  }
  if(ast->as_binop.type == BIN_DIV){
    result += calc_ast_various[ast->as_binop.left->type](ast->as_binop.left) / calc_ast_various[ast->as_binop.right->type](ast->as_binop.right);  
  }

  return result;
}

int64_t calc_ast_upon(struct AST *ast){
  int64_t result = 0;
  if(ast->as_unop.type == UN_NEG){
    result += -1 * calc_ast_various[ast->as_unop.operand->type](ast->as_unop.operand);
  }
  return result;
}

int64_t calc_ast_lit(struct AST *ast){
  return ast->as_literal.value;
}

int64_t calc_ast(struct AST *ast) {
  int64_t result = 0;
  if(ast)
    result += calc_ast_various[ast->type](ast);
  else
   printf("<NULL>");
  return result;
}

typedef void(p_printer)(FILE *, struct AST *);


static void p_print_binop(FILE *f, struct AST *ast) {
  fprintf(f, "(");
  print_ast(f, ast->as_binop.left);
  fprintf(f, ")");
  fprintf(f, "(");
  print_ast(f, ast->as_binop.right);
  fprintf(f, ")");
  fprintf(f, "%s", BINOPS[ast->as_binop.type]);
}
static void p_print_unop(FILE *f, struct AST *ast) {
  fprintf(f, "(");
  print_ast(f, ast->as_unop.operand);
  fprintf(f, ")");
  fprintf(f, "%s", UNOPS[ast->as_unop.type]);
}
static void p_print_lit(FILE *f, struct AST *ast) {
  fprintf(f, "%" PRId64, ast->as_literal.value);
}


static printer *ast_p_printers[] = {
    [AST_BINOP] = p_print_binop, [AST_UNOP] = p_print_unop, [AST_LIT] = p_print_lit };

void p_print_ast(FILE *f, struct AST *ast) {
  if(ast)
    ast_p_printers[ast->type](f, ast);
  else
   fprintf(f, "<NULL>");
}
